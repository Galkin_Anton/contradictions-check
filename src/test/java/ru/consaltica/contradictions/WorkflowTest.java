package ru.consaltica.contradictions;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.spring.boot.starter.test.helper.AbstractProcessEngineRuleTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class WorkflowTest extends AbstractProcessEngineRuleTest {

  @Autowired
  public RuntimeService runtimeService;

  @Test
  public void shouldExecuteHappyPath() {
    // given
    String processDefinitionKey = "contradictions-check-process";

    //TODO не забыть поправить тест с параметрами
    //ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processDefinitionKey);

    // Здесь нужно будет проверить успешное окончание
//    assertThat(processInstance).isStarted()
//        .task()
//        .hasDefinitionKey("say-hello")
//        .hasCandidateUser("demo")
//        .isNotAssigned();
  }

}
