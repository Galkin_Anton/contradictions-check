package ru.consaltica.contradictions.controller.tin;

import org.junit.Assert;
import org.junit.Test;

public class RussiaTinValidatorTest {

    @Test
    public void shouldbeTrue() {
        CountryTinValidator tinValidator = new RussiaTinValidator("5032111175", 2);
        Assert.assertTrue(tinValidator.isValid());

        tinValidator = new RussiaTinValidator("1181111110", 2);
        Assert.assertTrue(tinValidator.isValid());

        tinValidator = new RussiaTinValidator("1191111110", 2);
        Assert.assertTrue(tinValidator.isValid());

        //Для физлиц
        tinValidator = new RussiaTinValidator("500100732259", 1);
        Assert.assertTrue(tinValidator.isValid());
    }

    @Test
    public void shouldbeFalse() {
        CountryTinValidator tinValidator = new RussiaTinValidator("5032111176", 2);
        Assert.assertFalse(tinValidator.isValid());

        tinValidator = new RussiaTinValidator("1181111111", 2);
        Assert.assertFalse(tinValidator.isValid());

        tinValidator = new RussiaTinValidator("1191111111", 2);
        Assert.assertFalse(tinValidator.isValid());

        //Для физлиц
        tinValidator = new RussiaTinValidator("50010073225", 1);
        Assert.assertFalse(tinValidator.isValid());

        tinValidator = new RussiaTinValidator("500100732253", 1);
        Assert.assertFalse(tinValidator.isValid());

        tinValidator = new RussiaTinValidator("5001007322535", 1);
        Assert.assertFalse(tinValidator.isValid());
    }
}
