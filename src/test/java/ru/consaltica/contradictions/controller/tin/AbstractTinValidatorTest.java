package ru.consaltica.contradictions.controller.tin;

import org.junit.Assert;
import org.junit.Test;

/**
 * Project: contradictions-check
 * Author: Galkin A.B.
 * Date: 25.10.2020
 * Time: 17:14
 * Descriptions
 */

public class AbstractTinValidatorTest {
    @Test
    public void successPath() {
        CountryTinValidator ru = AbstractTinValidator.getCountryTinValidator("ru", "1", 1);
        Assert.assertFalse(ru.isValid());
    }
}
