package ru.consaltica.contradictions.controller.tin;

import org.camunda.bpm.engine.exception.NotFoundException;
import org.junit.Assert;
import org.junit.Test;

/**
 * Project: contradictions-check
 * Author: Galkin A.B.
 * Date: 25.10.2020
 * Time: 17:02
 * Descriptions
 */

public class CountrysInfoTest {

    @Test
    public void shouldbeRu() {
        CountrysInfo ru = CountrysInfo.getCounty("ru");
        Assert.assertEquals("RU", ru.toString());
    }

    @Test
    public void shouldbeRussiaTinValidatorClass() {
        CountrysInfo ru = CountrysInfo.getCounty("ru");
        CountryTinValidator countryTinValidator = ru.getCountryTinValidator("1", 1);
        Assert.assertEquals(RussiaTinValidator.class.getName(), countryTinValidator.getClass().getName());
    }

    @Test
    public void shouldbeNotFoundError() {
        try {
            CountrysInfo ru = CountrysInfo.getCounty("rus");
        } catch (NotFoundException e) {
            Assert.assertEquals("Страна не найдена", e.getMessage());
        }
    }
}
