package ru.consaltica.contradictions.controller.tin;

/**
 * Интерфейс API для валидации
 * ИНН
 */

public interface CountryTinValidator {

    boolean isValid();
}
