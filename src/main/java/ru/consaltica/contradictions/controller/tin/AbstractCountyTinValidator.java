package ru.consaltica.contradictions.controller.tin;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * Абстрактный класс родитель для классов валидаторов
 * контракт на проперти и методы, которые недоступны через общий интерфейс
 */
@Setter
@Getter
@RequiredArgsConstructor
public abstract class AbstractCountyTinValidator implements CountryTinValidator {
    /**
     * ИНН
     */
    private final String tin;
    /**
     * Для кого проверяется TIN
     * <p>
     * 1 - ФЛ/ИЛ
     * <p>
     * 2 - ЮЛ
     */
    private final int entity;

    abstract boolean isValidLegal();

    abstract boolean isValidIndividuals();

    @Override
    public boolean isValid() {
        boolean isValid = false;
        if (getEntity() == 2) {
            isValid = isValidLegal();
        } else if (getEntity() == 1) {
            isValid = isValidIndividuals();
        }

        return isValid;
    }
}
