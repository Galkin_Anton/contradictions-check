package ru.consaltica.contradictions.controller.tin;

import java.util.regex.Pattern;

/**
 * Интерфейс API для валидации
 * ИНН
 * <p>
 * 1.Алгоритм проверки 10-го значного ИНН.
 * <p>
 * ИНН.10. 1)Находим произведения первых 9-ти цифр ИНН на спепиальные множители соотственно. 9 множителей ( 2 4 10 3 5 9 4 6 8 ).
 * <p>
 * ИНН.10. 2) Складываем все 9-ть получившихся произведений.
 * <p>
 * ИНН.10. 3) Получившуюся сумму делим на число 11 и извлекаем целую часть частного от деления.
 * <p>
 * ИНН.10. 4) Умножаем получившееся число на 11.
 * <p>
 * ИНН.10. 5) Сравниваем числа получившиеся на шаге 2 и шаге 4, их разница, и есть контрольное число, которое и должно равняться 10-й цифре в ИНН. (Если контрольное число получилось равным 10-ти, в этом случае принимаем контрольное число равным 0.)
 * <p>
 * <p>
 * 2.Алгоритм проверки 12-го значног ИНН.
 * <p>
 * ИНН.12. 1)Находим произведения первых 10-ти цифр ИНН на спепиальные множители соотственно (10-ю цифру принимаем за 0). 10 множителей ( 7 2 4 10 3 5 9 4 6 8 ).
 * <p>
 * ИНН.12. 2) Складываем все 10-ть получившихся произведений.
 * <p>
 * ИНН.12. 3) Получившуюся сумму делим на число 11 и извлекаем целую часть частного от деления.
 * <p>
 * ИНН.12. 4) Умножаем получившееся число на 11. ИНН.12. 5) Сравниваем числа получившиеся на шаге 2 и шаге 4, их разница, и есть первое контрольное число, которое и должно равняться 11-й цифре в ИНН.(Если контрольное число получилось равным 10-ти, в этом случае принимаем контрольное число равным 0.) Если получившееся число не не равно 11-ой цифре ИНН, значит ИНН не верный, если же совпадает, тогда высчитываем следующее контрольное число, которое должно быть равным 12-ой цифре ИНН
 * <p>
 * ИНН.12. 6)Находим произведения первых 11-ти цифр ИНН на спепиальные множители соотственно (10-ю цифру принимаем за 0). 11 множителей ( 3 7 2 4 10 3 5 9 4 6 8 ).
 * <p>
 * ИНН.12. 7) Складываем все 11-ть получившихся произведений.
 * <p>
 * ИНН.12. 8) Получившуюся сумму делим на число 11 и извлекаем целую часть частного от деления.
 * <p>
 * ИНН.12. 9) Умножаем получившееся число на 11.
 * <p>
 * ИНН.12. 10) Сравниваем числа получившиеся на шаге 7 и шаге 9, их разница, и есть контрольное число, которое и должно равняться 12-й цифре в ИНН. (Если контрольное число получилось равным 10-ти, в этом случае принимаем контрольное число равным 0.) Если высчитанное число равно 12-ой цифре ИНН, и на первом этапе все контрольное число совпало с 11-ой цифрой ИНН, следовательно ИНН считается верным.
 */
public class RussiaTinValidator extends AbstractCountyTinValidator implements CountryTinValidator {

    public RussiaTinValidator(String tin, int entity) {
        super(tin, entity);
    }

    @Override
    public boolean isValid() {
        if (!Pattern.matches("^\\d+$", getTin())) {
            return false;
        }
        return super.isValid();
    }

    /**
     * Валидация ИНН для ЮЛ
     * Проверяется последняя контрольная цифра
     *
     * @return валдиный ли ИНН
     */
    @Override
    boolean isValidLegal() {
        if (getTin().length() != 10) {
            return false;
        }

        int[] multiplies = {2, 4, 10, 3, 5, 9, 4, 6, 8};
        var sum = multiply(multiplies, getTin());

        var checkValue = sum % 11 % 10;
        var lastValue = Character.getNumericValue(getTin().charAt(9));

        return checkValue == lastValue;
    }

    /**
     * Проверяются уже 2 последние цифры
     *
     * @return
     */
    @Override
    boolean isValidIndividuals() {
        if (getTin().length() != 12) {
            return false;
        }

        //Находим первую контрольную сумму и затес извлекаем цифру
        int[] multiplies = {7, 2, 4, 10, 3, 5, 9, 4, 6, 8};
        var sum = multiply(multiplies, getTin());
        var checkValueFirst = sum % 11 % 10;
        var prelastValue = Character.getNumericValue(getTin().charAt(10));

        //Находим вторую контрольную сумму и затес извлекаем цифру
        multiplies = new int[] {3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8};
        sum = multiply(multiplies, getTin());
        var checkValueSecond = sum % 11 % 10;
        var lastValue = Character.getNumericValue(getTin().charAt(11));

        return checkValueFirst == prelastValue && checkValueSecond == lastValue;
    }

    private int multiply(int[] multiplies, String tin) {
        var sum = 0;
        //Проходим по массиву и берем произведение
        for (int i = 0; i < multiplies.length; i++) {
            sum += multiplies[i] * Character.getNumericValue(getTin().charAt(i));
        }
        return sum;
    }
}
