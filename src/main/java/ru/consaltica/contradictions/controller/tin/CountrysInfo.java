package ru.consaltica.contradictions.controller.tin;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.SneakyThrows;
import org.camunda.bpm.engine.exception.NotFoundException;

import java.lang.reflect.Constructor;

@AllArgsConstructor
@Getter
public enum CountrysInfo {
    RU(RussiaTinValidator.class, true, "Россия", "999999999999 для ФЛ и 9999999999 для ЮЛ");

    private final Class tinValidatorClass;
    private final boolean isRequeredTin;
    private final String countryName;
    private final String formatTin;

    /**
     * @param countyIsoName принимает на вход дату в формате ISO3166-1 alpha-2 (2 буквы)
     * @return CountrysToTinValidator
     */
    public static CountrysInfo getCounty(String countyIsoName) {
        CountrysInfo country = null;
        try {
            country = CountrysInfo.valueOf(countyIsoName.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new NotFoundException("Страна не найдена", e);
        }
        return country;
    }

    @SneakyThrows
    public CountryTinValidator getCountryTinValidator(String tin, int entity) {
        Constructor<CountryTinValidator> constr = tinValidatorClass.getConstructor(String.class, int.class);
        return constr.newInstance(tin, entity);
    }
}
