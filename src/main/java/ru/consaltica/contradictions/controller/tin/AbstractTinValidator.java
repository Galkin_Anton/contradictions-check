package ru.consaltica.contradictions.controller.tin;

public abstract class AbstractTinValidator {
    /**
     * API для валидации ИНН по стране
     *
     * @param country Страна в формате ISO Alpha-2
     * @param tin     TIN
     * @param entity  ЮЛ или ФЛ
     * @return экземпляр имплементации интерфейса {@link CountrysInfo}
     */
    public static CountryTinValidator getCountryTinValidator(String country, String tin, int entity) {
        return CountrysInfo.getCounty(country).getCountryTinValidator(tin, entity);
    }
}
