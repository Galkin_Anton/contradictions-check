package ru.consaltica.contradictions.service;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import ru.consaltica.contradictions.ProcessConstants;
import ru.consaltica.contradictions.model.ContradictionsRequest;
import ru.consaltica.contradictions.model.ContradictionsResult;

import java.util.HashMap;

@Slf4j
@RestController
@Service
@RequestMapping("/start")
public class ProcessStarter {
    private final static String PROCESS_KEY = "contradictions-check-process";

    @Autowired
    public RuntimeService runtimeService;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ContradictionsResult processStart(@RequestBody ContradictionsRequest requestBody) {
        var createdProcess = createProcess(requestBody);
        return createdProcess.
          getVariables().
          getValue(ProcessConstants.P_CONTRADICTIONS_RESULT, ContradictionsResult.class);
    }

    private ProcessInstanceWithVariables createProcess(ContradictionsRequest requestBody) {
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put(ProcessConstants.P_TIN, requestBody.getTin());
        stringObjectHashMap.put(ProcessConstants.P_COUNTRY, requestBody.getCountry());
        stringObjectHashMap.put(ProcessConstants.P_ENTITY, requestBody.getEntity());
        stringObjectHashMap.put(ProcessConstants.P_TIN_TYPE, requestBody.getTinType());
        stringObjectHashMap.put(ProcessConstants.P_CLIENT_ANSWER, requestBody.getClientAnswer());

        return runtimeService
          .createProcessInstanceByKey(PROCESS_KEY)
          .setVariables(stringObjectHashMap)
          .executeWithVariablesInReturn();
    }
}
