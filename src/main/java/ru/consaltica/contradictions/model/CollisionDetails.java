package ru.consaltica.contradictions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class CollisionDetails implements Serializable {

    @JsonProperty("CollisionMarker")
    private String collisionMarker;

    @JsonProperty("CollisionProofDoc")
    private String collisionProofDoc;

    private boolean isProofDocExists;
}
