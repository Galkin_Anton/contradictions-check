package ru.consaltica.contradictions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ContradictionsResult implements Serializable {

    @JsonProperty("RequestStatus")
    private int requestStatus;

    @JsonProperty("ErrorCode")
    private int errorCode;

    @JsonProperty("CollisionDetails")
    private CollisionDetails collisionDetails;

    @JsonProperty("DocRecomendations")
    private DocRecomendations docRecomendations;

    @JsonProperty("TinValidation")
    private List<TinValidation> tinValidation;
}
