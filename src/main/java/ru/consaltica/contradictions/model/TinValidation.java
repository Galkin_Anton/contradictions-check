package ru.consaltica.contradictions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor
@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class TinValidation implements Serializable {
    @JsonProperty("TINCountry")
    private String tinCountry;

    @JsonProperty("TINNumber")
    private String tinNumber;

    @JsonProperty("TINType")
    private String tinType;

    private boolean isValid;
}
