package ru.consaltica.contradictions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class DocRecomendations implements Serializable {
    
    @JsonProperty("Description")
    private String description;

    @JsonProperty("DocName")
    private String docName;
}
