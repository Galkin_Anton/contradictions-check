package ru.consaltica.contradictions.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ContradictionsRequest {

    @JsonAlias("CountryCD")
    private String country;

    @JsonAlias("TIN")
    private String tin;

    /**
     * Для кого проверяется TIN
     * <p>
     * 1 - ФЛ/ИЛ
     * <p>
     * 2 - ЮЛ
     */
    @JsonAlias("Entity")
    private int entity;
    /**
     * TIN
     * <p>
     * OTHER
     * <p>
     * Если не задан или задан TIN то идет проверка как TIN
     * иначе идет проверка как аналога
     */
    @JsonAlias("TinType")
    private String tinType;

    @JsonAlias("ClientAnswer")
    private String clientAnswer;
}
