package ru.consaltica.contradictions;

/**
 * Project: contradictions-check
 * Author: Galkin A.B.
 * Date: 25.10.2020
 * Time: 18:41
 * Descriptions
 */

public interface ProcessConstants {
    String P_VALIDATION_MESSAGE = "validationMessage";
    String P_IS_TIN_VALID = "isTinValid";
    String P_TIN = "tin";
    String P_COUNTRY = "country";
    String P_ENTITY = "entity";
    String P_CONTRADICTIONS_RESULT = "contradictionsResult";
    String P_IS_REQUERED_TIN = "isRequeredTin";
    String P_TIN_TYPE = "tinType";
    String P_CLIENT_ANSWER = "clientAnswer";
}
