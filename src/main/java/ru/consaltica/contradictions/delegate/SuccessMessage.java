package ru.consaltica.contradictions.delegate;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
import ru.consaltica.contradictions.ProcessConstants;
import ru.consaltica.contradictions.model.ContradictionsResult;

/**
 * Формирование успешного сообщения о завершении работы сервиса
 */
@Component
public class SuccessMessage implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        var contradictionsResult = new ContradictionsResult();
        contradictionsResult.setRequestStatus(0);
        contradictionsResult.setErrorCode(0);

        execution.setVariable(ProcessConstants.P_CONTRADICTIONS_RESULT, contradictionsResult);
    }
}
