package ru.consaltica.contradictions.delegate.contradictions;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
import ru.consaltica.contradictions.ProcessConstants;
import ru.consaltica.contradictions.controller.tin.CountrysInfo;
import ru.consaltica.contradictions.model.CollisionDetails;
import ru.consaltica.contradictions.model.ContradictionsResult;
import ru.consaltica.contradictions.model.DocRecomendations;

/**
 * Project: contradictions-check
 * Author: Galkin A.B.
 * Date: 25.10.2020
 * Time: 19:56
 * Descriptions
 */
@Slf4j
@Component
public class Contradiction101_1 implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        var country = (String) execution.getVariable(ProcessConstants.P_COUNTRY);
        var countryInfo = CountrysInfo.getCounty(country);
        var contradictionsResult = new ContradictionsResult();
        var docRecomendations = new DocRecomendations();
        var collisionDetails = new CollisionDetails();

        contradictionsResult.setDocRecomendations(docRecomendations);
        contradictionsResult.setCollisionDetails(collisionDetails);

        docRecomendations.setDescription(
          "Предоставленный TIN для страны/территории <название страны> не валиден.  Требуется запросить у клиента корректный TIN (или его аналог) иначе открыть счет/совершить операцию будет невозможно. "
            + countryInfo.getFormatTin());
        docRecomendations.setDocName("none");
        collisionDetails.setCollisionMarker("101 - Значение TIN не прошло валидацию");
        collisionDetails.setProofDocExists(false);
        contradictionsResult.setRequestStatus(201);
        contradictionsResult.setErrorCode(882);

        execution.setVariable(ProcessConstants.P_CONTRADICTIONS_RESULT, contradictionsResult);
    }
}
