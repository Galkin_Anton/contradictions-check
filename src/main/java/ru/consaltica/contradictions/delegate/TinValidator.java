package ru.consaltica.contradictions.delegate;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
import ru.consaltica.contradictions.ProcessConstants;
import ru.consaltica.contradictions.controller.tin.AbstractTinValidator;
import ru.consaltica.contradictions.controller.tin.CountryTinValidator;
import ru.consaltica.contradictions.controller.tin.CountrysInfo;

/**
 * Project: contradictions-check
 * Author: Galkin A.B.
 * Date: 25.10.2020
 * Time: 17:16
 * Descriptions
 */
@Slf4j
@Component
public class TinValidator implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        var tin = (String) execution.getVariable(ProcessConstants.P_TIN);
        var country = (String) execution.getVariable(ProcessConstants.P_COUNTRY);
        var entity = (int) execution.getVariable(ProcessConstants.P_ENTITY);
        log.info("Start validate TIN for country = {}, TIN = {}, entity = {}", country, tin, entity);

        //Получаем эезхемпляр валидационного класса
        CountryTinValidator countryTinValidator = AbstractTinValidator.getCountryTinValidator(country, tin, entity);
        var isTinValid = countryTinValidator.isValid();

        log.info("End validate TIN for country = {}, TIN = {}, entity = {}, is valid = {}", country, tin, entity, isTinValid);

        execution.setVariable(ProcessConstants.P_IS_TIN_VALID, isTinValid);
        execution.setVariable(ProcessConstants.P_IS_REQUERED_TIN, CountrysInfo.getCounty(country).isRequeredTin());
        if (!isTinValid) {
            execution.setVariable(ProcessConstants.P_VALIDATION_MESSAGE, "TIN не валиден");
        }
    }
}
